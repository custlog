class CreateEntries < ActiveRecord::Migration
  def self.up
    create_table :entries do |t|
      t.text :complaint
      t.text :resolution
      t.date :created_on

      t.timestamps
    end
  end

  def self.down
    drop_table :entries
  end
end
