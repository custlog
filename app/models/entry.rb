class Entry < ActiveRecord::Base
  validates_presence_of :complaint,
                        :resolution
end
