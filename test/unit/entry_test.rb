require File.dirname(__FILE__) + '/../test_helper'

class EntryTest < ActiveSupport::TestCase
  
  def test_should_create_record
    entry = create
    assert entry.valid?, "Entry was invalid:\n#{entry.to_yaml}"
  end
  
  def test_should_require_complaint
    entry = create(:complaint => nil)
    assert_not_nil entry.errors.on(:complaint), "No resolution should add an error"
  end
  
  def test_should_require_resolution
    entry = create(:resolution => nil)
    assert_not_nil entry.errors.on(:resolution), "No resolution should add an error"
  end
  
  def test_should_not_change_date_on_edit
    
  end
  
private

  def create(options={})
    Entry.create(@@entry_default_values.merge(options))
  end
  
end
