require "#{File.dirname(__FILE__)}/../test_helper"

class EntryStoriesTest < ActionController::IntegrationTest
  
  def test_create_new_entry
    get "/entries/new"
    assert_response :success
    assert_template "new"
  end
  
  def test_edit_entry
    get "/entries/edit/#{entries(:one).id}"
    assert_response :success
    assert_template "edit"
    
    post "/entries/update", :id => entries(:one).id, :complaint => "test complaint", :resolution => "test resolution"
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_template "show"
  end
  
end
